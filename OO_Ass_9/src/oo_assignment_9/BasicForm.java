package oo_assignment_9;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class BasicForm implements Form {
    
    private boolean value;

    public BasicForm( boolean value ) {
        this.value = value;
    }

    @Override
    public <R> R accept( FormVisitor<R> v ) {
        return v.visit( this );
    }

    public boolean getValue() {
        return value;
    }

}
