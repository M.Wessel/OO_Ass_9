package oo_assignment_9;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class AtomicForm implements Form {

    private String id;

    public AtomicForm( String id ) {
        this.id = id;
    }

    @Override
    public <R> R accept( FormVisitor<R> v ) {
        return v.visit( this );
    }

    public String getID() {
        return id;
    }

}
