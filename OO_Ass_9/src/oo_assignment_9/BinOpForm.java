package oo_assignment_9;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class BinOpForm implements Form {

    private BinOp op;
    private Form leftOperand;
    private Form rightOperand;

    public BinOpForm( BinOp op, Form left, Form right ) {
        this.op = op;
        this.leftOperand = left;
        this.rightOperand = right;
    }

    public Form getLeft() {
        return leftOperand;
    }

    public Form getRight() {
        return rightOperand;
    }

    public BinOp getOp() {
        return op;
    }

    @Override
    public <R> R accept( FormVisitor<R> v ) {
        return v.visit( this );
    }
}
