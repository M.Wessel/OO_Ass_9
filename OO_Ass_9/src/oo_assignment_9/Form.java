package oo_assignment_9;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public interface Form {
    /**
     * Method to apply the FormVisitor to the class
     */
    public <R> R accept( FormVisitor<R> v );
}
