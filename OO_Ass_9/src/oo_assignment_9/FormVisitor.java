package oo_assignment_9;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public interface FormVisitor<R> {
    
    /**
     * The visit function for a BinOpForm
     * @param form: a binary operand formula (e.g. "A /\ B" )
     */
    R visit( BinOpForm form );
    
    /**
     * The visit function for a NotForm
     * @param form: a negating formula (e.g. "~A" )
     */
    R visit( NotForm form );
    
    /**
     * The visit function for a AtomicForm
     * @param form: an atomic formula (e.g. "A" )
     */
    R visit( AtomicForm form );
    
    /**
     * The visit function for a BasicForm
     * @param form: a basic formula (e.g. "true" )
     */
    R visit( BasicForm form );
}
