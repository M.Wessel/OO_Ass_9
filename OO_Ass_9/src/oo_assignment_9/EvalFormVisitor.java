package oo_assignment_9;

import java.util.Map;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class EvalFormVisitor implements FormVisitor {
    private Map<String,Boolean> environ;
    
    public EvalFormVisitor( Map<String, Boolean> environ ) {
        this.environ = environ;
    }
    
    @Override
    public Boolean visit( BinOpForm form ) {
        Boolean left = (Boolean) form.getLeft().accept( this );
        Boolean right = (Boolean) form.getRight().accept( this );
        return form.getOp().apply(left, right);
    }

    @Override
    public Boolean visit( NotForm form ) {
        Boolean value = (Boolean) form.getOperand().accept( this );
        return !value;
    }

    @Override
    public Boolean visit( AtomicForm form ) {
        return environ.get( form.getID() );
    }

    @Override
    public Boolean visit( BasicForm form ) {
        return form.getValue();
    }
}
