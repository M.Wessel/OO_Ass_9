package oo_assignment_9;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */

public enum BinOp {
    AndOp( "/\\" ) {
        public boolean apply( boolean b1, boolean b2 ) {
            return b1 && b2;
        }
    },
    OrOp( "\\/" ) {
        public boolean apply( boolean b1, boolean b2) {
            return b1 || b2;
        }
    },
    ImpliesOp( "=>" ) {
        public boolean apply( boolean b1, boolean b2 ) {
            return !b1 || b2;
        }
    };
    
    private String string;
    
    private BinOp( String string ) {
        this.string = string;
    }

    //This general version of the "apply" function is needed for the visitor,
    //but it is never used in this way
    boolean apply( boolean left, boolean right ) {
        throw new UnsupportedOperationException( "Not supported yet." );
    }
    
    @Override
    public String toString( ) {
        return string;
    }
}
