package oo_assignment_9;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class NotForm implements Form {
    private Form operand;
    
    public NotForm( Form oper ) {
        this.operand = oper;
    }
    
    public Form getOperand() {
        return operand;
    }

    @Override
    public <R> R accept( FormVisitor<R> v ) {
        return v.visit( this );
    }
    
}
