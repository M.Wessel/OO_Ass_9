package oo_assignment_9;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class PrintVisitor implements FormVisitor {

    @Override
    public Boolean visit( BinOpForm form ) {
        System.out.print( "(" );
        form.getLeft().accept( this );
        System.out.print( " " + form.getOp() + " " );
        form.getRight().accept( this );
        System.out.print( ")" );        
        return null;
    }

    @Override
    public Boolean visit( NotForm form ) {
        System.out.print( "~" );
        form.getOperand().accept( this );
        return null;
    }

    @Override
    public Boolean visit( AtomicForm form ) {
        System.out.print( form.getID() );
        return null;
    }

    @Override
    public Boolean visit( BasicForm form ) {
        System.out.print( form.getValue() );
        return null;
    }

    


    
    
    
    
    
}
