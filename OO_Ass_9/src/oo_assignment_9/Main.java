package oo_assignment_9;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main( String[] args ) {

        Map<String, Boolean> store = new HashMap<>();
        store.put( "A", true );
        store.put( "B", true );
        store.put( "C", false );

        FormVisitor printVisitor = new PrintVisitor();
        FormVisitor evalVisitor = new EvalFormVisitor( store );

        ArrayList<Form> trees = new ArrayList<Form>();
        trees.add( tree1() );
        trees.add( tree2() );
        trees.add( tree3() );
        trees.add( tree4() );
        trees.add( tree5() );
        trees.add( tree6() );

        for( Form tree : trees ) {
            tree.accept( printVisitor );
            System.out.println( "\n" + tree.accept( evalVisitor ) );
        }

    }

    /**
     * A /\ B \/ B /\ C
     * True /\ True \/ True /\ False
     * 
     * Expected result = True
     */
    public static Form tree1() {
        Form AandB = new BinOpForm( BinOp.AndOp, new AtomicForm( "A" ), new AtomicForm( "B" ) );
        Form BandC = new BinOpForm( BinOp.AndOp, new AtomicForm( "B" ), new AtomicForm( "C" ) );

        return new BinOpForm( BinOp.OrOp, AandB, BandC );
    }

    /**
     * True -> ~A
     * 
     * Expected result = False
     */
    public static Form tree2() {
        return new BinOpForm(
                BinOp.ImpliesOp, new BasicForm( true ), new NotForm( new AtomicForm( "A" ) )
        );
    }

    /**
     * (~((A=>B)/\(B\/C))\/((A=>B)/\(B\/C)))
     * 
     * Expected result = true
     */
    public static Form tree3() {
        Form AimpliesB = new BinOpForm(
                BinOp.ImpliesOp, new AtomicForm( "A" ), new AtomicForm( "B" )
        );

        Form BorC = new BinOpForm(
                BinOp.OrOp, new AtomicForm( "B" ), new AtomicForm( "C" )
        );

        Form AimpliesBandBorC = new BinOpForm(
                BinOp.AndOp, AimpliesB, BorC
        );

        return new BinOpForm(
                BinOp.OrOp, new NotForm( AimpliesBandBorC ), AimpliesBandBorC
        );
    }
    
    /**
     * A /\ B => false
     * 
     * Expected result = false
     */
    public static Form tree4(){
        Form AandC = new BinOpForm(
                BinOp.AndOp, new AtomicForm( "A" ), new AtomicForm( "B" )
        );
        
        return new BinOpForm(
                BinOp.ImpliesOp, AandC, new BasicForm( false )
        );
    }
    
    /**
     * (A => C) /\ (A => B)
     * 
     * Expected result = false
     */
    public static Form tree5(){
        Form AimpliesC = new BinOpForm(
                BinOp.ImpliesOp, new AtomicForm( "A" ), new AtomicForm( "C" )
        );
        
        Form AimpliesB = new BinOpForm(
                BinOp.ImpliesOp, new AtomicForm( "A" ), new AtomicForm( "B" )
        );
        
        return new BinOpForm(
                BinOp.AndOp, AimpliesC, AimpliesB
        );
    }
    
    /**
     * Negation of tree5
     * 
     * Expected result = true
     */
    public static Form tree6(){
        return new NotForm(tree5());
    }
    
    
    
    
    

}
